import express from 'express'
const router = express.Router()
import { Code, createResult } from '../../db/common-model/result'
import { createToken } from '../../utils/token'
import { md5 } from '../../utils'
import { TokenType } from '../../db/token'
import Admin from '../../db/admin/index'

/**
 * @api {Get} /auth/adminLogin adminLogin 管理员登录
 * @apiGroup auth 权限
 *
 * @apiParam {String} username=fanzhiliang777 管理员名称
 * @apiParam {String} password=123456 密码
 *
 * @apiUse Result
 * @apiSuccess {String} data 返回的token
 */
router.get('/', async(req, res, next) => {
  const result = createResult()

  const query = req.query
  const username = query.username as string
  const password = query.password as string
  const md5Password = md5(password)

  if (username && md5Password) {
    try {
      const admin = await Admin.getAdminByUsernameAndPassword({
        username,
        password: md5Password,
      })

      const isValidate = admin?.username === username && admin?.password === md5Password

      if (isValidate) {
        result.code = Code.Success
        result.data = await createToken({
          userId: admin?._id,
          username,
          password: md5Password,
          type: TokenType.Admin,
        })
      } else {
        result.code = Code.DataError
        result.msg = '管理员名称或密码错误'
      }
    } catch (error) {
      next(error)
    }
  } else {
    result.code = Code.DataError
    result.msg = '请输入管理员名称和密码'
  }

  res.send(result)
})

export default router
