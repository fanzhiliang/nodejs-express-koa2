import express from 'express'
const router = express.Router()
import TokenFilter from '../../filters/token'
import { createResult } from '../../db/common-model/result'
import { AdminModel } from '../../db/admin/index'

router.use(TokenFilter.use(TokenFilter.admin))

/**
 * @api {Get} /admin/getAdminInfo getAdminInfo 获取管理员信息
 * @apiGroup admin 管理员
 *
 * @apiUse Authorization
 *
 * @apiUse Result
 * @apiUse UserSuccess
 */
router.get('/', (req, res) => {
  const result = createResult()

  const query = req.query
  const admin = query.admin as AdminModel

  // 删除隐私字段
  // delete admin._id
  delete admin.password

  result.data = admin

  res.send(result)
})

export default router
