import express from 'express'
import Admin, { AdminModel } from '../../db/admin'
import { Code, createResult } from '../../db/common-model/result'
import TokenFilter from '../../filters/token'
import { md5 } from '../../utils'
const router = express.Router()

router.use(TokenFilter.use(TokenFilter.admin))

/**
 * @api {Post} /admin/updatePassword updatePassword 修改管理员密码
 * @apiGroup admin 管理员
 *
 * @apiUse Authorization
 *
 * @apiParam {String} password 旧密码
 * @apiParam {String} newPassword 新密码
 *
 * @apiUse Result
 */

router.post('/', async(req, res, next) => {
  const result = createResult()

  const query = req.query
  const body = req.body

  // 接口调用者
  const authAdmin = query.admin as AdminModel

  const password = body.password as string
  const md5Password = md5(password)
  const newPassword = body.newPassword as string

  let errMsg = ''

  if (!password || !password.trim()) {
    errMsg = '请输入旧密码'
  } else if (!newPassword || !newPassword.trim()) {
    errMsg = '请输入新密码'
  } else if (password === newPassword) {
    errMsg = '新密码与旧密码不能相同'
  }

  if (errMsg) {
    result.code = Code.DataError
    result.msg = errMsg
  } else {
    try {
      const admin = await Admin.getAdminByUsernameAndPassword({
        username: authAdmin.username,
        password: md5Password,
      })

      const isValidate = admin?.username === authAdmin.username && admin?.password === md5Password

      if (isValidate) {
        await Admin.updateOneAdmin({
          _id: admin?._id,
        }, {
          password: md5Password,
        })
        result.code = Code.Success
        result.msg = '修改成功'
      } else {
        result.code = Code.DataError
        result.msg = '旧密码错误'
      }
    } catch (error) {
      result.code = Code.DataError
      result.msg = error
      next(error)
    }
  }

  res.send(result)
})

export default router
