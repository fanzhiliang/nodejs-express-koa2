import express from 'express'
import { AdminModel } from '../../db/admin'
import { Code, createResult } from '../../db/common-model/result'
import User, { UserModel } from '../../db/user'
import TokenFilter from '../../filters/token'
import { md5 } from '../../utils'
const router = express.Router()

router.use(TokenFilter.or(TokenFilter.user, TokenFilter.admin))

/**
 * @api {Post} /user/updatePassword updatePassword 修改用户密码
 * @apiGroup user 用户
 *
 * @apiUse Authorization
 *
 * @apiParam {String} userId 用户id
 * @apiParam {String} password 旧密码
 * @apiParam {String} newPassword 新密码
 *
 * @apiUse Result
 */
router.post('/', async(req, res, next) => {
  const result = createResult()

  const query = req.query
  const body = req.body

  // 接口调用者
  const authUser = query.user as UserModel
  const authAdmin = query.admin as AdminModel

  // 修改的用户id
  const userId = authUser && authUser._id ? authUser._id : (body.userId as string)
  const password = body.password as string
  const md5Password = md5(password)
  const newPassword = body.newPassword as string

  let errMsg = ''

  if (!newPassword || !newPassword.trim()) {
    errMsg = '请输入新密码'
  }

  if (authUser) {
    if (!password || !password.trim()) {
      errMsg = '请输入旧密码'
    } else if (password === newPassword) {
      errMsg = '新密码与旧密码不能相同'
    }
  } else if (authAdmin) {
    if (!userId) {
      errMsg = '请输入用户id'
    }
  }

  if (errMsg) {
    result.code = Code.DataError
    result.msg = errMsg
  } else {
    try {
      const user = await User.getUserById({
        _id: userId,
      })

      // 如果是管理员操作 或者 用户名和密码相同 就验证成功
      const isValidate = authAdmin || (user?.username === authUser.username && user?.password === md5Password)

      if (isValidate) {
        await User.updateOneUser({
          _id: userId,
        }, {
          password: md5(newPassword),
        })
        result.code = Code.Success
        result.msg = '修改成功'
      } else {
        result.code = Code.DataError
        result.msg = '旧密码错误'
      }
    } catch (error) {
      result.code = Code.DataError
      result.msg = error
      next(error)
    }
  }

  res.send(result)
})

export default router
