import express from 'express'
const router = express.Router()
import TokenFilter from '../../filters/token'
import Order from '../../db/order'
import { UserModel } from '../../db/user'
import { Code, createResult } from '../../db/common-model/result'

router.use(TokenFilter.or(TokenFilter.user, TokenFilter.admin))

/**
 * @api {Get} /order/getOrderList getOrderList 获取订单列表
 * @apiGroup order 订单
 *
 * @apiUse Authorization
 *
 * @apiUse ListParams
 *
 * @apiUse Result
 * @apiUse ListResult
 * @apiSuccess {Order[]} data.list 订单数组 (看下方json)
 *
 * @apiUse OrderSuccessExample
 * @apiUse GoodsSuccessExample
 */

router.get('/', async(req, res, next) => {
  const result = createResult()

  const query = req.query
  const user = query.user as UserModel

  const userId = user && user._id ? user._id as string : ''

  try {
    const data = await Order.getOrderList(userId, {
      current: Number(query.current),
      size: Number(query.size),
      sort: query.sort as string,
      order: query.order as string,
    })
    // 查询成功
    result.code = Code.Success
    result.data = data
  } catch (error) {
    // 报错
    result.code = Code.DataError
    result.msg = error.toString()
    next(error)
  }

  res.send(result)
})

export default router
