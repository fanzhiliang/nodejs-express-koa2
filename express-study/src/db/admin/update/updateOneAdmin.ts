import { Admin, AdminModel } from '../index'

export const updateOneAdmin = function(
  this: typeof Admin,
  filter: AdminModel,
  admin: AdminModel,
) {
  return this.updateOne(filter, admin)
}
