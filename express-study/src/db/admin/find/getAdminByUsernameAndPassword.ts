import { Admin, AdminModel } from '../index'

export const getAdminByUsernameAndPassword = function(
  this: typeof Admin,
  admin: AdminModel,
) {
  return this.findOne({
    username: admin.username,
    password: admin.password,
  }).lean()
}
