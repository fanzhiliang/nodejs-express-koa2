/**
 * 全局变量
*/
import * as config from './src/config'

type Config = typeof config

declare global {
  namespace NodeJS {
    interface Global {
      Config: Config
    }
  }
}
