import { ValueTransformer } from 'xl-ts-plugins/typeorm'
import { md5, parseTime } from './index'

/**
 * 实体类字段转换工具
*/

// 密码转化器
export const passwordTransformer: ValueTransformer = {
  to: val => md5(val),
  from: () => '',
}

// 数值转化器
export const fromNumberTransformer: ValueTransformer = {
  to: val => val,
  from: val => Number(val || 0),
}

// 当前时间转化器
export const toValOrNowTransformer: ValueTransformer = {
  to: val => val || parseTime(new Date()),
  from: val => val,
}