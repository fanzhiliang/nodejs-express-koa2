import { Result } from 'xl-ts-plugins/router'
export { ajaxLogFail } from 'xl-ts-plugins/router'
import { ApiModel, ApiModelProperty, DataType } from 'xl-ts-plugins/swagger'

/**
 * 自定义响应格式
*/

export const ResultModelName = 'Result'

@ApiModel({ name: ResultModelName, description: '普通响应格式', isCreateTag: false })
export class CustomResult<T = any> extends Result<T> {
  @ApiModelProperty({ description: '状态码', type: DataType.Number, default: 200 })
  code: number

  @ApiModelProperty({ description: '数据', type: DataType.Object })
  data: T

  // @ApiModelProperty({ description: '信息', type: DataType.String, default: '' })
  // msg: string

  @ApiModelProperty({ description: '信息', type: DataType.String, default: '' })
  message: string

  @ApiModelProperty({ description: '是否成功', type: DataType.Boolean, default: true })
  success: boolean

  constructor(code = 500, data = null, message = '', success = true) {
    super(code, data, '')
    this.code = code
    this.data = data
    this.message = message
    this.success = success
    // 删掉没用的字段
    delete this.msg
  }
}

export function ajaxResultSuccess<T extends any>(data: T, msg = ''): CustomResult<T> {
  return new CustomResult(200, data, msg, true)
}

export function ajaxResultFail(msg = '', code = 500): CustomResult<any> {
  return new CustomResult(code, null, msg, false)
}