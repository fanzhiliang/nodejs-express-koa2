import { Entity, PrimaryGeneratedColumn, Column } from 'xl-ts-plugins/typeorm'
import { ApiModel, ApiModelProperty, DataType } from 'xl-ts-plugins/swagger'
import { toValOrNowTransformer } from '../utils/entity-transformer'

@ApiModel({
  name: 'UserAmountRecord',
  description: '用户金额变动记录'
})
@Entity('user_amount_record')
export class UserAmountRecord {

  @ApiModelProperty({ description: '用户金额变动记录id', type: DataType.Number })
  @PrimaryGeneratedColumn({ name: 'user_amount_record_id' })
  userAmountRecordId: number


  @ApiModelProperty({ description: '用户余额id', type: DataType.Number })
  @Column({ name: 'user_amount_id' })
  userAmountId: number


  @ApiModelProperty({ description: '变动金额', type: DataType.Number })
  @Column({ name: 'change_amount' })
  changeAmount: number


  @ApiModelProperty({ description: '记录描述', type: DataType.String })
  @Column({ name: 'description' })
  description: string


  @ApiModelProperty({ description: '创建时间', type: DataType.String })
  @Column({
    name: 'create_time',
    transformer: toValOrNowTransformer
  })
  createTime: string
}