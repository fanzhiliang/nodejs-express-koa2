import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany } from 'xl-ts-plugins/typeorm'
import { User } from './User'
import { Photo } from './Photo'
import { ApiModel, ApiModelProperty, DataType } from 'xl-ts-plugins/swagger'

@ApiModel({
  name: 'Author',
  description: '作者'
})
@Entity('author')
export class Author {

  @ApiModelProperty({ description: '作者id', type: DataType.Number })
  @PrimaryGeneratedColumn({ name: 'author_id' })
  authorId: number


  @ApiModelProperty({ description: '用户id', type: DataType.Number })
  @Column({ name: 'user_id' })
  userId: number


  @ApiModelProperty({ description: '用户', type: DataType.Model('User') })
  @OneToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User


  @ApiModelProperty({ description: '介绍', type: DataType.String })
  @Column({ name: 'introduction' })
  introduction: string


  @ApiModelProperty({ description: '照片列表', type: DataType.ModelArray('Photo') })
  @OneToMany(() => Photo, (photo) => photo.author)
  photos: Photo[]
}