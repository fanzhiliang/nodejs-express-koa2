import { Entity, PrimaryGeneratedColumn, Column } from 'xl-ts-plugins/typeorm'
import { ApiModel, ApiModelProperty, DataType } from 'xl-ts-plugins/swagger'
import { fromNumberTransformer, toValOrNowTransformer } from '../utils/entity-transformer'

@ApiModel({
  name: 'UserAmount',
  description: '用户金额'
})
@Entity('user_amount')
export class UserAmount {

  @ApiModelProperty({ description: '用户金额id', type: DataType.Number })
  @PrimaryGeneratedColumn({ name: 'user_amount_id' })
  userAmountId: number


  @ApiModelProperty({ description: '用户id', type: DataType.Number })
  @Column({ name: 'user_id' })
  userId: number


  @ApiModelProperty({ description: '余额', type: DataType.Number })
  @Column({
    name: 'amount',
    type: 'decimal',
    transformer: fromNumberTransformer
  })
  amount: number


  @ApiModelProperty({ description: '创建时间', type: DataType.String })
  @Column({
    name: 'create_time',
    transformer: toValOrNowTransformer
  })
  createTime: string
}