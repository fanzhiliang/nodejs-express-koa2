import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'xl-ts-plugins/typeorm'
import { Author } from './Author'
import { ApiModel, ApiModelProperty, DataType } from 'xl-ts-plugins/swagger'
import { toValOrNowTransformer } from '../utils/entity-transformer'


@ApiModel({
  name: 'Photo',
  description: '照片'
})
@Entity('photo')
export class Photo {

  @ApiModelProperty({ description: '照片id', type: DataType.Number })
  @PrimaryGeneratedColumn({ name: 'photo_id' })
  photoId: number


  @ApiModelProperty({ description: '作者id', type: DataType.Number })
  @Column({ name: 'author_id' })
  authorId: number


  @ApiModelProperty({ description: '作者', type: DataType.Model('Author') })
  @ManyToOne(() => Author, (author) => author.photos)
  @JoinColumn({ name: 'author_id' })
  author: Author


  @ApiModelProperty({ description: '描述', type: DataType.String })
  @Column({ name: 'description' })
  description: string


  @ApiModelProperty({ description: '链接', type: DataType.String })
  @Column({ name: 'url' })
  url: string


  @ApiModelProperty({ description: '创建时间', type: DataType.String })
  @Column({
    name: 'create_time',
    transformer: toValOrNowTransformer
  })
  createTime: string
}