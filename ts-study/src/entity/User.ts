import { Entity, PrimaryGeneratedColumn, Column } from 'xl-ts-plugins/typeorm'
import { passwordTransformer } from '../utils/entity-transformer'
import { ApiModel, ApiModelProperty, DataType } from 'xl-ts-plugins/swagger'

// 状态
export enum UserStatus {
  // 禁用
  Disable = 0,
  // 正常
  Normal = 1,
}

// 性别
export enum UserGender {
  // 未知
  Unknown = 0,
  // 男
  Male = 1,
  // 女
  Female = 2,
}

@ApiModel({ name: 'User', description: '用户' })
@Entity('user')
export class User {

  @ApiModelProperty({ description: '用户id', type: DataType.Number })
  @PrimaryGeneratedColumn({ name: 'user_id' })
  userId: number


  @ApiModelProperty({ description: '用户名', type: DataType.String })
  @Column({ name: 'user_name' })
  userName: string


  @ApiModelProperty({ description: '密码', type: DataType.String })
  @Column({
    name: 'user_password',
    transformer: passwordTransformer
  })
  userPassword: string


  @ApiModelProperty({ description: '电话', type: DataType.String })
  @Column({ name: 'user_phone' })
  userPhone: string


  @ApiModelProperty({ description: '状态 默认1 (0:禁用  1:正常)', type: DataType.Number })
  @Column({
    name: 'user_status',
    type: 'enum',
    enum: UserStatus,
    default: UserStatus.Normal,
  })
  userStatus: UserStatus


  @ApiModelProperty({ description: '年龄', type: DataType.Number })
  @Column({ name: 'user_age' })
  userAge: number


  @ApiModelProperty({ description: '性别 默认0 (0:未知  1:男  2:女)', type: DataType.Number })
  @Column({
    name: 'user_gender',
    type: 'enum',
    enum: UserGender,
    default: UserGender.Unknown,
  })
  userGender: UserGender


  @ApiModelProperty({ description: '地址', type: DataType.String })
  @Column({ name: 'user_address' })
  userAddress: string


  @ApiModelProperty({ description: '头像', type: DataType.String })
  @Column({ name: 'user_avatar' })
  userAvatar: string
}

