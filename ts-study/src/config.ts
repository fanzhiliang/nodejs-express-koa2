import path from 'path'

// 服务器地址
export const HOST = process.env.HOST

// 运行端口
export const PORT = process.env.PORT

// token 名称
export const TOKEN_KEY = 'Authorization'

// token 密钥
export const TOKEN_SECRET = process.env.TOKEN_SECRET

// token 过期时间
export const TOKEN_EXP = 1000 * 60 * 60 * 24 * 366

// 资源文件目录
export const RESOURCES_PATH = path.resolve(__dirname, '../resources')

// 自己导入自己
import * as Config from './config'

global.Config = Config