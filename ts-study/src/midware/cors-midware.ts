import { Request, Response, NextFunction } from 'express'

/**
 * 跨域中间件
*/
export default function (req: Request, res: Response, next: NextFunction) {
  // 设置允许跨域的域名，*代表允许任意域名跨域
  res.header('Access-Control-Allow-Origin', '*')
  /**
   * 允许的header类型
   * 要将添加的请求头设置到这里，不然随意添加请求头会出现跨域的问题
  */
  res.header('Access-Control-Allow-Headers', `Content-Type, Content-Length, Accept, ${global.Config.TOKEN_KEY}`)
  // 跨域允许的请求方式
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, POST, GET, OPTIONS')

  next()
}