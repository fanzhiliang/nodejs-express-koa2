import { ApiModel, ApiController, ApiPost } from 'xl-ts-plugins/swagger'
import redis from 'xl-ts-plugins/redis'
import { User } from '../entity/User'
import { ajaxResultSuccess } from '../utils/custom-result'

/**
 * redis 测试
*/
@ApiModel({
  name: 'RedisTest', description: 'redis 测试', isCreateModel: false
})
@ApiController({ path: '/redisTest', tags: ['RedisTest'] })
export default class RedisTestController {
  @ApiPost({ path: '/baseTest', summary: '测试' })
  async baseTest() {
    redis.client.del('testKey')

    // 字符串测试
    // redis.set('testKey', 123)

    // redis.get('testKey').then(value => {
    //   console.log(value)
    //   console.log(typeof value)
    // })

    // 对象测试
    const user = {
      userId: 9,
      userName: 'fanzhiliang',
      userPhone: '13128269543'
    }

    redis.hmset('testKey', user)

    redis.hmget('testKey', User).then(user => {
      console.log(user)
    })

    // 数组
    // const list = ['1', '2', 3]

    // redis.setList('testKey', list, true)

    // redis.getList('testKey').then(list => {
    //   console.log(list);
    // })

    return ajaxResultSuccess('测试')
  }
}