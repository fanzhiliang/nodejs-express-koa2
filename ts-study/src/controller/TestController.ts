import { ApiModel, ApiController, ApiPost, ApiGet, ApiBodyParam, ApiQueryParam, ApiParamsParam, DataType, ApiCustomPost, Query, Body, Request, Response, ExperssResponse, ExperssRequest, DataFormat } from 'xl-ts-plugins/router'
import TokenUserMidware from '../midware/token-user-midware'
import UploadMidware from '../midware/upload-midware'
import fs from 'fs'
import { ajaxResultFail, ajaxResultSuccess, ajaxLogFail } from '../utils/custom-result'

/**
 * 测试
*/
@ApiModel({
  name: 'Test', description: '测试', isCreateModel: false
})
@ApiController({ path: '/test', tags: ['Test'] })
export default class TestController {

  @ApiGet({ path: '/', summary: '测试' })
  async test() {
    return ajaxResultSuccess('测试')
  }


  @ApiGet({
    path: '/get', summary: 'get测试',
    responseList: [
      { name: 'name', description: '名称' }
    ]
  })
  async testGet(
    @Query query: any,
    @ApiQueryParam({ name: 'name', type: DataType.String, description: '名称' }) name: string
  ) {
    if (!name) {
      return ajaxResultFail('名称不能为空')
    }
    return ajaxResultSuccess(query)
  }


  @ApiPost({
    path: '/post', summary: 'post测试',
    responseList: [
      { name: 'age', type: DataType.Number, description: '年龄' }
    ]
  })
  async testPost(
    @Body body: any,
    @ApiBodyParam({ name: 'age', type: DataType.Number, description: '年龄' }) age: number
  ) {
    if (!age) {
      ajaxLogFail('输入正确的年龄')
    }
    return ajaxResultSuccess(body)
  }


  @ApiGet({ path: '/param/:id/:name', summary: 'param测试' })
  async param(
    @ApiParamsParam({ name: 'id', type: DataType.String }) id: string,
    @ApiParamsParam({ name: 'name', type: DataType.String }) name: string
  ) {
    return ajaxResultSuccess({ id, name })
  }

  @ApiCustomPost(
    {
      path: '/uploadSingle',
      summary: '单个文件上传测试',
      dataFormat: DataFormat.FORMDATA_CUSTOM,
      parameterList: [
        { name: 'file', type: DataType.File, in: 'formData' }
      ]
    },
    TokenUserMidware.midware,
    UploadMidware.single('file')
  )
  async uploadSingle(@Request req: ExperssRequest, @Response res: ExperssResponse) {
    const filePath = UploadMidware.getSingleFilePath(req)

    if (fs.existsSync(filePath)) {
      try {
        const rs = fs.createReadStream(filePath)

        rs.on('data', chunk => {
          res.write(chunk)
        })

        rs.on('end', () => {
          res.status(200)
          res.end()
        })
      } catch (error) {
        return ajaxResultFail('无法读取文件')
      }
    } else {
      return ajaxResultFail('文件不存在')
    }
  }


  @ApiCustomPost(
    {
      path: '/uploadMultiple',
      summary: '多个文件上传测试',
      dataFormat: DataFormat.FORMDATA_CUSTOM,
      parameterList: [
        { name: 'files', type: DataType.File, in: 'formData' }
      ]
    },
    TokenUserMidware.midware,
    UploadMidware.multiple([
      { name: 'files', maxCount: 3 }
    ])
  )
  async uploadMultiple(@Request req: ExperssRequest) {
    const list = UploadMidware.getMultipleFilePath(req)

    return ajaxResultSuccess(list)
  }

}