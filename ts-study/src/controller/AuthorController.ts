import { ApiController, ApiGet, DataType } from 'xl-ts-plugins/router'
import { getRepository } from 'xl-ts-plugins/typeorm'
import { Author } from '../entity/Author'
import { ajaxResultSuccess } from '../utils/custom-result'

/**
 * 作者控制器
*/
@ApiController({
  path: '/author', tags: ['Author']
})
export default class AuthorController {

  @ApiGet({
    path: '/getAuthorList', summary: '获取全部作者列表',
    responseModel: { type: DataType.ModelArray('Author') }
  })
  async getAuthorList() {
    const result = await getRepository(Author)
      .createQueryBuilder('author')
      .leftJoinAndSelect('author.user', 'user')
      .leftJoinAndSelect('author.photos', 'photo')
      .getMany()

    return ajaxResultSuccess(result)
  }
}