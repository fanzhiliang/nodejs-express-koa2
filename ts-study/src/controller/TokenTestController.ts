import { ApiModel, ApiController, ApiPost, ApiBody, ApiBodyParam } from 'xl-ts-plugins/swagger'
import { TokenData, createToken, parseToken } from 'xl-ts-plugins/token'
import { ajaxResultSuccess } from '../utils/custom-result'

/**
 * token 测试
*/
@ApiModel({
  name: 'TokenTest', description: 'token测试', isCreateModel: false
})
@ApiController({ path: '/tokenTest', tags: ['TokenTest'] })
export default class TokenTestController {

  @ApiPost({ path: '/createToken', summary: '创建 token' })
  async createToken(@ApiBody() body: TokenData) {
    const token = await createToken(body)

    return ajaxResultSuccess(token)
  }


  @ApiPost({ path: '/parseToken', summary: '解析 token' })
  async parseToken(@ApiBodyParam({ name: 'token' }) token: string) {
    const tokenData = await parseToken(token)

    return ajaxResultSuccess(tokenData)
  }
}